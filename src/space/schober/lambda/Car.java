package space.schober.lambda;

public class Car implements Comparable<Car> {
	private String name;
	private int topSpeed;
	private int numberOfSeats;

	public Car(String name, int topSpeed, int numberOfSeats) {
		this.setName(name);
		this.setTopSpeed(topSpeed);
		this.setNumberOfSeats(numberOfSeats);
	}

	@Override
	public int compareTo(Car otherCar) {
		if (this.getNumberOfSeats() != otherCar.getNumberOfSeats())
			return otherCar.getNumberOfSeats() - this.getNumberOfSeats();

		if (!this.getName().equals(otherCar.getName()))
			return this.getName().compareTo(otherCar.getName());

		if (this.getTopSpeed() != otherCar.getTopSpeed())
			return otherCar.getTopSpeed() - this.getTopSpeed();

		return 0;
	}

	@Override
	public String toString() {
		return String.format("%20s: %03d km/h & %01d seats", this.getName(), this.getTopSpeed(),
				this.getNumberOfSeats());
	}

	// Getter & Setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTopSpeed() {
		return topSpeed;
	}

	public void setTopSpeed(int topSpeed) {
		this.topSpeed = topSpeed;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}
}
