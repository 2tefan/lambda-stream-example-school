package space.schober.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class App {

	public static void main(String[] args) {
		List<Car> cars = new ArrayList<>();
		cars.add(new Car("Seat Ibiza", 160, 5));
		cars.add(new Car("VW T-Roc", 190, 5));
		cars.add(new Car("Ferrari LaFerrari", 360, 2));
		cars.add(new Car("Fiat Multipla", 170, 6));
		printCars(cars);

		System.out.println("Sort - w/o Lambda");
		Collections.sort(cars);
		printCars(cars);

		System.out.println("Sort - w/o Lambda - with custom comparator");
		Collections.sort(cars, new Comparator<Car>() {
			@Override
			public int compare(Car car1, Car car2) {
				return car2.getTopSpeed() - car1.getTopSpeed();
			}
		});
		printCars(cars);

		System.out.println("Sort - w/ Lambda & custom comparator");
		cars.sort((Car car1, Car car2) -> car2.getTopSpeed() - car1.getTopSpeed());
		printCars(cars);

		System.out.println("Sort - w/ Lambda & custom comparator");
		cars.sort((Car car1, Car car2) -> {
			return car2.getTopSpeed() - car1.getTopSpeed();
		});
		printCars(cars);

		System.out.println("Sort - w/ Lambda & print output");
		cars.stream().sorted().forEach(car -> System.out.println(car));
		System.out.println();

		System.out.println("Sort - w/ method reference operator");
		cars.stream().sorted().forEach(System.out::println);
		System.out.println();

		System.out.println("Sort - w/ method reference operator & custom comparator");
		cars.stream().sorted((Car car1, Car car2) -> car2.getTopSpeed() - car1.getTopSpeed())
				.forEach(System.out::println);

	}

	private static void printCars(List<Car> cars) {
		for (Car car : cars) {
			System.out.println(car);
		}
		System.out.println();
	}

}
